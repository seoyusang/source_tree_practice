import React from "react";
import { withStyles } from "@material-ui/core/styles";
import ListItem from "@material-ui/core/ListItem";
import Divider from "@material-ui/core/Divider";
import ListItemText from "@material-ui/core/ListItemText";
import Typography from "@material-ui/core/Typography";
import Truncate from "react-truncate";

const useStyles = (theme) => ({
  inline: { 
    fontSize: 16,
    display: "inline",
  }, 
  buttonMargin: {
    display: "inline",
    marginRight: theme.spacing(2),
  },
});
function timeForToday(value) {
  const today = new Date();
  const timeValue = new Date(value);

  const betweenTime = Math.floor((today.getTime() - timeValue.getTime()) / 1000 / 60);
  if (betweenTime < 1) return '방금전';
  if (betweenTime < 60) {
      return `${betweenTime}분전`;
  }

  const betweenTimeHour = Math.floor(betweenTime / 60);
  if (betweenTimeHour < 24) {
      return `${betweenTimeHour}시간전`;
  }

  const betweenTimeDay = Math.floor(betweenTime / 60 / 24);
  if (betweenTimeDay < 365) {
      return `${betweenTimeDay}일전`;
  }

  return `${Math.floor(betweenTimeDay / 365)}년전`;
}

class NewsListItem extends React.Component {
  componentDidMount() {
    this.props
      .callapi()
      .then((res) => {
        this.props.set_news(res);
      })
      .catch((err) => console.log(err));
  }

  render() {
    const { classes } = this.props;
    return (
      <div>
        {this.props.news
          ? this.props.news.map((c) => {
              return (
                <div>
                  <ListItem alignItems="flex-start">
                    <ListItemText
                      primary={c.title}
                      secondary={
                        <React.Fragment>
                          <div>
                            <Truncate
                              lines={2}
                              ellipsis={
                                <span>
                                  ...
                                  <a href={c.original_url} target="_blank">
                                    더보기
                                  </a>
                                </span>
                              }
                            >
                              {c.content}
                            </Truncate>
                          </div>
                          <div class="inline">
                            <Typography

                              component="span"
                              variant="body2"
                              className={classes.buttonMargin}
                              color="textPrimary">
                              {c.writer}
                            </Typography>
                            <Typography
                              component="span"
                              variant="body2"
                              className={classes.inline}
                              color="error">
                              {timeForToday(c.post_date)}
                            </Typography>
                          </div>
                        </React.Fragment>
                      }
                    />
                  </ListItem>
                  <Divider />
                </div>
              );
            })
          : ""}
      </div>
    );
  }
}

export default withStyles(useStyles)(NewsListItem);
