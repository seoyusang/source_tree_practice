import React from "react"
import { FormControl, InputLabel, Select} from "@material-ui/core"
import { withStyles } from '@material-ui/core/styles';

const useStyles = (theme) => ({
  formControl: {
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
});

class NativeSelect extends React.Component {
  
  render(){
    const { classes } = this.props;

    return (
      <FormControl variant="outlined" className={classes.formControl}>
        <InputLabel htmlFor="outlined-age-native-simple">새로고침</InputLabel>
        <Select
          native
          onChange={this.props.selectChange}
          label="새로고침"
          inputProps={{
            name: 'age',
            id: 'outlined-age-native-simple',
          }}
        >
            <option aria-label="None" value="" />
            <option value={20}>20초마다</option>
            <option value={40}>40초마다</option>
            <option value={60}>1분마다</option>
            <option value={180}>3분마다</option>
            <option value={300}>5분마다</option>
            <option value={600}>10분마다</option>
        </Select>
      </FormControl>
      )
  }

}
export default withStyles(useStyles)(NativeSelect);