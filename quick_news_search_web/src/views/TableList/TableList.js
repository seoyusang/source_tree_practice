import React, { useState, useEffect } from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
// core components
import NewsListItem from "components/NewsListItem";
import NativeSelect from "components/NativeSelect";

import { Scrollbars } from "react-custom-scrollbars";
// core components
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";
import Search from "@material-ui/icons/Search";
import styles from "assets/jss/material-dashboard-react/components/searchLinksStyle.js";

import AdSense from "react-adsense";

const useStyles = makeStyles(styles);

export default function TableList() {
  const [news, setnews] = useState("");
  useEffect(() => {
    if (timer) clearInterval(timer);
    if (refreshTime) {
      const timer = setInterval(() => {
        autoRefreshEvent();
      }, refreshTime * 1000);
      setTimer(timer);
      return () => clearTimeout(timer);
    }
  }, [news]);

  const [searchWord, setsearchWord] = useState("");
  useEffect(() => {}, [searchWord]);

  const [staticSearchWord, setstaticSearchWord] = useState("");
  useEffect(() => {}, [staticSearchWord]);

  const [timer, setTimer] = useState("");

  const [refreshTime, setrefreshTime] = useState("");
  useEffect(() => {
    if (refreshTime) {
      const timer = setInterval(() => {
        autoRefreshEvent();
      }, refreshTime * 1000);
      setTimer(timer);
      return () => clearTimeout(timer);
    }
  }, [refreshTime]);

  var topOfList = React.createRef();
  const scrollToTop = () => {
    if (topOfList.current) {
      topOfList.current.scrollIntoView();
    }
  };

  const classes = useStyles();

  const selectChange = (e) => {
    setrefreshTime(e.target.value);
  };

  const callApiFront = async function (searchWord) {
    const response = await fetch(
      "/get_news_data_search_front?searchWord=" + searchWord
    );
    const body = await response.json();
    return body;
  };

  const callApiRefresh = async function () {
    const response = await fetch(
      searchWord
        ? "/get_news_data_refresh?searchWord=" +
            searchWord +
            "&first_index=" +
            news[0].index
        : "/get_news_data_refresh?first_index=" + news[0].index,
      { headers: { "Cache-Control": "no-cache" } }
    );
    const body = await response.json();
    return body;
  };

  function autoRefreshEvent() {
    callApiRefresh()
      .then((res) => Refresh_state(res))
      .catch((err) => console.log(err));
  }

  const inputChange = (e) => {
    setsearchWord(e.target.value);
  };

  const callApiSearch = async function (staticSearchWord) {
    const response = await fetch(
      "/get_news_data_search?searchWord=" + staticSearchWord
    );
    const body = await response.json();
    return body;
  };

  const Search_state = (res) => {
    setnews((news) => [...res]);
  };

  const Refresh_state = (res) => {
    setnews([...res, ...news]);
    // bug... why don't append to news array?
  };

  function onClick() {
    scrollToTop();
    setstaticSearchWord(searchWord);
    callApiSearch(searchWord)
      .then((res) => Search_state(res))
      .catch((err) => console.log(err));
  }

  const Append_state = (res) => {
    setnews((news) => [...news, ...res]);
  };

  const callApi = async () => {
    try {
      const response = await fetch(
        "/get_news_data_add_bottom?last_index=" +
          Object.values(news.slice(-1)[0])[0] +
          "&searchWord=" +
          staticSearchWord
      );
      const body = await response.json();
      return body;
    } catch (error) {
      console.log(error);
      const response = await fetch("/get_news_data_add_bottom");
      const body = await response.json();
      return body;
    }
  };

  function onScroll(e) {
    const target = e.target;
    if (target.scrollHeight - target.scrollTop < target.clientHeight + 1) {
      callApi()
        .then((res) => Append_state(res))
        .catch((err) => console.log(err));
    }
  }

  return (
    <div>
      <div className={classes.searchWrapper} style={{ marginLeft: "16px" }}>
        <CustomInput
          formControlProps={inputChange}
          inputProps={{
            placeholder: "검색",
            inputProps: {
              "aria-label": "검색",
            },
          }}
        />
        <Button
          classes={classes}
          color="white"
          aria-label="edit"
          justIcon
          round
          onClick={onClick}
        >
          <Search />
        </Button>
        <NativeSelect classes={classes} selectChange={selectChange} />
      </div>

      <Scrollbars
        autoHide
        onScroll={onScroll}
        style={{
          marginTop: "10px",
          width: "100%",
          height: "75vh",
          paddingright: "15px",
        }}
      >
        {<span ref={topOfList} />}
        <NewsListItem news={news} set_news={Append_state} callapi={callApi} />
      </Scrollbars>
    </div>
  );
}
