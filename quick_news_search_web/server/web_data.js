let express = require("express");
let router = express.Router();
var mysql = require("mysql");

var connection = mysql.createConnection({
  host: "quick-news-search-db.cfi03ikusnxf.ap-northeast-2.rds.amazonaws.com",
  //host: "nanaba.cnfagikgkquy.ap-northeast-2.rds.amazonaws.com",
  user: "admin",
  database: "QUICK_NEWS_DB",
  password: "rkdtjddns!23",
  port: 3306,
  multipleStatements: true,
  dateStrings: "date",
});

//가장 최근 뉴스 순으로 30개 추출 반환
//http://15.165.148.172:3000/get_news_data
router.get("/get_news_data/", function (req, res) {
  var sql =
    "SELECT * FROM NEWS WHERE NEWS.index IS NOT NULL ORDER BY NEWS.index DESC LIMIT 30";
  var params = [];
  connection.query(sql, params, function (err, result) {
    if (err) console.log(err);
    else res.json(result);
  });
});

//가장 최근 뉴스 순으로 검색어 기반 30개 추출 반환
//http://15.165.148.172:3000/get_news_data_search
router.get("/get_news_data_search/", function (req, res) {
  var searchWord = "%" + req.query.searchWord + "%";
  var sql =
    "SELECT * FROM NEWS WHERE NEWS.index IS NOT NULL AND title LIKE ? AND content LIKE ? ORDER BY NEWS.index DESC LIMIT 30";

  var params = [searchWord, searchWord];
  connection.query(sql, params, function (err, result) {
    console.log(sql);
    if (err) console.log(err);
    else res.json(result);
  });
});

//스크롤시 아래 추가데이터 불러오기
//http://15.165.148.172:3000/get_news_data_add_bottom
router.get("/get_news_data_add_bottom/", function (req, res) {
  var last_index = Number(req.query.last_index);
  var searchWord = req.query.searchWord;

  var sql = "";
  var params = [];
  if (!last_index && !searchWord)
    sql =
      "SELECT * FROM NEWS WHERE NEWS.index IS NOT NULL ORDER BY NEWS.index DESC LIMIT 30;";
  else if (!searchWord) {
    sql =
      "SELECT * FROM NEWS WHERE NEWS.index IS NOT NULL AND NEWS.index < ? ORDER BY NEWS.index DESC LIMIT 30;";
    params = [last_index];
  } else {
    searchWord = "%" + searchWord + "%";
    sql =
      "SELECT * FROM NEWS WHERE NEWS.index IS NOT NULL AND NEWS.index < ? AND title LIKE ? AND content LIKE ? ORDER BY NEWS.index DESC LIMIT 30;";
    params = [last_index, searchWord, searchWord];
  }
  connection.query(sql, params, function (err, result) {
    console.log(sql);
    if (err) console.log(err);
    else res.json(result);
  });
});

//스크롤시 아래 추가데이터 불러오기
//http://15.165.148.172:3000/get_news_data_add_bottom
router.get("/get_news_data_refresh/", function (req, res) {
  var first_index = Number(req.query.first_index);
  var searchWord = req.query.searchWord;

  var sql = "";
  var params = [];
  if (!searchWord) {
    sql =
      "SELECT * FROM NEWS WHERE NEWS.index IS NOT NULL AND NEWS.index > ? ORDER BY NEWS.index DESC;";
    params = [first_index];
  } else {
    searchWord = "%" + searchWord + "%";
    sql =
      "SELECT * FROM NEWS WHERE NEWS.index IS NOT NULL AND NEWS.index > ? AND title LIKE ? AND content LIKE ? ORDER BY NEWS.index DESC;";
    params = [first_index, searchWord, searchWord];
  }
  connection.query(sql, params, function (err, result) {
    console.log(sql);
    if (err) console.log(err);
    else res.json(result);
  });
});
module.exports = router;
