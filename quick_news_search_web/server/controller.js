//Web 관련 
var createError = require('http-errors');
const bodyParser = require("body-parser");
//var cookieParser = require('cookie-parser');
var logger = require('morgan');
//const methodOverride = require('method-override');


//Express Setting
//var path = require('path');
var express = require('express');

//Router 파일 추가
var webRouter = require('./web_data');
var app = express();


//Web 관련 view engine setup
//app.set('views', path.join(__dirname, 'views'));
//app.set('view engine', 'html');
app.use(express.urlencoded({ extended: true }));
app.use(bodyParser.json()); //here
//app.use(express.static(path.join(__dirname, 'public')));
//app.use(methodOverride('_method'));
app.use(logger('dev'));
app.use(express.json());
//app.use(cookieParser());
//const passport = require('passport');
//app.use(passport.initialize());
//passportConfig();


//Router 경로설정
app.use('/', webRouter);


// 에러 핸들링 : catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send('error');
});

module.exports = app;

