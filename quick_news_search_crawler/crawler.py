import pymysql
import requests
from bs4 import BeautifulSoup
import pandas as pd
from datetime import datetime
import time

Speed_News = "https://news.naver.com/main/list.nhn?mode=LSD&mid=sec&sid1=대분류"
Normal_News = "https://news.naver.com/main/list.nhn?mode=LS2D&mid=shm&sid1=대분류&sid2=소분류"

Main_Categori = (("정치", "100") , ("경제","101"), ("사회","102"), ("생활/문화", "103"),("세계","104"),("IT/과학","105"))
Sub_Categori = (("100" ,"청와대", "264"), ("100" ,"국회/정당", "265"), ("100" ,"북한", "268"), ("100" ,"행정", "266"), ("100" ,"국방/외교", "267"), ("100" ,"정치일반", "269")
                ,("101" ,"금융", "259"),("101" ,"증권", "258"),("101" ,"산업/재계", "261"),("101" ,"중기/벤처", "771"),("101" ,"부동산", "260"),("101","글로벌경제", "262"),("101","생활경제", "310"),("101","경제일반", "263")
                ,("102" ,"사건사고", "249"),("102" ,"교육", "250"),("102" ,"노동", "251"),("102" ,"언론", "254"),("102" ,"환경", "252"),("102","인권/복지", "59b"),("102","식품/의료", "255"),("102","지역", "256"),("102","인물", "276"),("102","사회일반", "257")
                ,("103" ,"건강정보", "241"),("103" ,"자동차/시승기", "239"),("103" ,"도로/교통", "240"),("103" ,"여행/레저", "237"),("103" ,"음식/맛집", "238"),("103","패션/뷰티", "376"),("103","공연/전시", "242"),("103","책", "243"),("103","종교", "244"),("103","날씨", "248"),("103","생활문화 일반", "245")
                ,("104" ,"아시아/호주", "231"),("104" ,"미국/중남미", "232"),("104" ,"유럽", "233"),("104" ,"중동/아프리카", "234"),("104" ,"세계 일반", "322")
                ,("105" ,"모바일", "731"),("105" ,"인터넷/SNS", "226"),("105" ,"통신/뉴미디어", "227"),("105" ,"IT 일반", "230"),("105" ,"보안/해킹", "732"),("105" ,"컴퓨터", "283"),("105" ,"게임/리뷰", "229"),("105" ,"과학 일반", "228"))


conn = pymysql.connect(host='quick-news-search-db.cfi03ikusnxf.ap-northeast-2.rds.amazonaws.com', user='admin', password='rkdtjddns!23', db='QUICK_NEWS_DB', charset='utf8')

#1차 크롤링 : 네이버의 리스트에서 추가되는 뉴스들을 전문과 Original URL 그리고 기사 게시날짜를 제외하고 다 가져옴 (10분 이내에 추가된 데이터와 비교하여 이미 추가된 데이터는 INSERT 진행을 하지 않음)
def crawler():
    #f = open("D:/python study/beautifulSoup_ws/crawling_result/contents_text.txt", 'w', encoding='utf-8')
    
    cursor = conn.cursor() 

    #LIST에서 뉴스 리스트를 가져올 URL 수립
    URLS = []
    for MC in Main_Categori:  
        for SC in Sub_Categori:
            if MC[1] in SC:
                Temp = Normal_News.replace("대분류", MC[1])
                Temp = Temp.replace("소분류", SC[2])
                URLS.append((Temp,MC[0],SC[1]))
    
    for MC in Main_Categori:      
        Temp = Speed_News.replace("대분류", MC[1])
        URLS.append((Temp,MC[0],"속보"))
    

    #URL을 통해 뉴스기사 수집
    List_Result = []

    list_count = 0
    for URL in URLS:
        #print(URL[0])
        req = requests.get(URL[0])
        cont = req.content
        soup = BeautifulSoup(cont, 'html.parser')
    
        for li in soup.find("ul",{"class": "type06_headline"}).find_all("li")+soup.find("ul",{"class": "type06"}).find_all("li"):
            big_category = URL[1]
            sub_category = URL[2]
            naver_url = li.find_all("a")[0]['href'].replace('\n','')
            try:
                title = li.find_all("a")[1].string.replace('\t','').replace('\n','')[2:]
            except Exception as e:
                title = li.find_all("a")[0].string.replace('\t','').replace('\n','')[2:]
            writer = li.find("span", {"class": "writing"}).string.replace('\n','')
            List_Result.append((naver_url, big_category, sub_category, title, writer))
            #print((naver_url, big_category, sub_category,  title, writer))
            list_count = list_count +1
            print("기사 수집 중... " + str(list_count))
        
        time.sleep(0.3)


    #DB전체 데이터 가져온다음 위에서 크롤링 한 데이터와 비교 후 DB에 있는 데이터는 삭제
    sql = "select naver_url, big_category, sub_category, title, writer from NEWS where scrap_date > DATE_ADD(now(), INTERVAL -10 MINUTE)"
    cursor.execute(sql)    
    rows = cursor.fetchall()

    count_deleted = 0
    for row in rows:
        if row in List_Result:
            #print(str(List_Result[List_Result.index(row)]) + "is deleted")
            del List_Result[List_Result.index(row)]
            count_deleted = count_deleted + 1 
            print("10분내 추가된 기사들과 현재 크롤링하는 기사 대조 후 삭제 중... " + str(count_deleted))
        
    
    
    count = 0
    #데이터 INSERT
    sql1 = "INSERT INTO NEWS (naver_url, big_category, sub_category,  title, writer, scrap_date) VALUES (%s, %s, %s, %s, %s, NOW())" 
    sql2 = "UPDATE NEWS SET big_category = %s, sub_category = %s,  title = %s, writer = %s WHERE naver_url = %s" 
    for list_result in List_Result:
        count = count + 1
        try:
            cursor.execute(sql1, (list_result[0], list_result[1], list_result[2], list_result[3], list_result[4]))
        except Exception as e:
            try:
                cursor.execute(sql2, (list_result[1], list_result[2], list_result[3], list_result[4], list_result[0]))
                print ("INSERT IS FAILED SO UPDATED" + str(list_result))
            except Exception as e:
                print ("Error %d: %s" % (e.args[0], e.args[1]))

        print("List 추가 현재완료상황", str(count)+ " / "+str(len(List_Result)))
    
    conn.commit()
    

def crawler_2():
    
    print("뉴스 전문 2차 크롤링 시작")
    #DB전체 데이터 가져온다음 위에서 크롤링 한 데이터와 비교 후 DB에 있는 데이터는 삭제
    cursor = conn.cursor() 
    sql = "select naver_url, big_category, sub_category, title, writer from NEWS where original_url is null"
    cursor.execute(sql)    
    rows = cursor.fetchall()

    
    count = 0
    sql1 = "UPDATE NEWS SET post_date = %s, content = %s,  original_url = %s WHERE naver_url = %s" 
    sql2 = "DELETE FROM NEWS WHERE naver_url = %s"
    sql3 = "SET @cnt = (SELECT MAX(NEWS.index) FROM NEWS)"
    sql4 = "UPDATE NEWS SET NEWS.index = (SELECT @cnt := @cnt + 1) WHERE NEWS.index IS NULL ORDER BY post_date"
    for row in rows:
        req = requests.get(row[0])
        cont = req.content
        soup = BeautifulSoup(cont, 'html.parser')

        #데이터 가져오기
        try:
            #2020.05.03. 오후 8:10 -> 2020-05-03 20:10:00
            str_date = soup.find("span",{"class": "t11"}).get_text().replace('\n','').replace('오후','PM').replace('오전','AM')
            convert_date = datetime.strptime(str_date, '%Y.%m.%d. %p %I:%M')
            result=[]
            result.append(row[0])
            result.append(convert_date)
            result.append(soup.find("div",{"id": "articleBodyContents"}).get_text().replace('\n',''))
            result.append(soup.find("div",{"class": "article_info"}).find_all("a")[0]['href'])
            try:
                cursor.execute(sql1, (result[1],result[2],result[3],result[0]))
            except Exception as e:
                print ("Error %d: %s" % (e.args[0], e.args[1]))
        except Exception as e:
            try:
                print ("NAVER NEWS IS DELETED SO DELETE" + str(row[0]))
                cursor.execute(sql2, (row[0]))
            except Exception as e:
                print ("Error %d: %s" % (e.args[0], e.args[1]))
       
        count = count + 1
        print("NEWS 전문 추가 현재완료상황", str(count)+ " / "+str(len(rows)))
        time.sleep(0.3)

    try:
        print ("INDEX IS INITIALIZED")
        cursor.execute(sql3, ())
        cursor.execute(sql4, ())
    except Exception as e:
        print ("Error %d: %s" % (e.args[0], e.args[1]))


    conn.commit()
        

def main():
    crawler()
    crawler_2()

main()
